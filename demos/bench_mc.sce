// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function mcprob ( nprob )
  shortname = intprb_getname(nprob);
  longname = intprb_getname(nprob,%t);
  mprintf("=======================\n");
  mprintf("Problem #%d : %s (%s)\n",nprob,longname,shortname);
  [ n , p ] = intprb_getsetup(nprob);
  e = intprb_getexpect(n,p,nprob);
  var = intprb_getvariance(n,p,nprob);
  if ( var == [] ) then
    mprintf("Expectation = %+10.5e\n",e);
  else
    mprintf("Expectation = %+10.5e, Variance =%10.7e\n",e,var);
  end
  mprintf("%10s %10s %10s %10s\n","m","Mean","Variance","Accuracy");
  for callf = logspace(1,5,5)
    [ integr , accur , varf ] = intprb_getcrudemc ( nprob , n , p , callf )
    mprintf("%10d %+10.5e %10.7e %10.7e\n",callf,integr,varf,accur);
  end
endfunction

//
//
// Do A Monte-Carlo on all problems
nprobmax = intprb_getproblems();
for nprob = 1 : nprobmax
  mcprob ( nprob )
end


//
// Load this script into the editor
//
filename = "bench_mc.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

