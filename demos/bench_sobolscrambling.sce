// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function u= ldsobolscrambled(callf,n,scrambling)
    // "" (no scrambling), 
    // "Owen" for the scrambling (digit permutation) of Owen, 
    // "Faure-Tezuka" or 
    // "Owen-Faure-Tezuka"
    lds = lowdisc_new("sobol");
    lds = lowdisc_configure(lds,"-dimension",n);
    lds = lowdisc_configure(lds,"-scrambling",scrambling);
    [lds,u] = lowdisc_next (lds,callf);
    lds = lowdisc_destroy(lds);
endfunction

//u= ldsobolscrambled(63,2,"Owen")
//plot(u(:,1),u(:,2),"bo")

function benchLD(nprob, n)
    body = [
    "y = intprb_getfcn ( callf , n , x , p , nprob );"
    ];
    prot=funcprot();
    funcprot(0);
    deff("y = func ( callf , n , x , p , nprob )",body);
    funcprot(prot);
    //
    npoints=2^(2:17);
    colmat=["bo-","ro-","go-","ko-"];
    scramblingmatrix=["","Owen","Faure-Tezuka","Owen-Faure-Tezuka"];
    for i=1:size(scramblingmatrix,"*")
        abserrLDS=[];
        for callf = npoints
            integr = intprb_crudeld ( func , n , ...
                list(ldsobolscrambled,scramblingmatrix(i)) , callf );
            abserrLDS($+1)=abs(integr-e);
        end
        plot(npoints',abserrLDS,colmat(i));
    end
    plot(npoints',1 ./(npoints'),"b-");
    a=gca();
    a.log_flags="lln";
    stitle=msprintf("%s - %d dimensions",longname,n);
    xtitle(stitle,..
    "Number of Simulations","Absolute Error");
    scramblingmatrix(1)="No Scramling"
    scramblingmatrix($+1)="1/n"
    legend(scramblingmatrix);
endfunction

nprob=17;
longname = intprb_getname(nprob,%t);
[n,p]=intprb_getsetup(nprob);
e = intprb_getexpect(n,p,nprob);
scf();
subplot(2,2,1)
benchLD(nprob,5);
subplot(2,2,2)
benchLD(nprob,10);
subplot(2,2,3)
benchLD(nprob,20);
subplot(2,2,4)
benchLD(nprob,40);
