// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

demopath = get_absolute_file_path("intprbs.dem.gateway.sce");
subdemolist = [
"genzphi", "genzphi.sce"; ..
"bench_mc", "bench_mc.sce"; ..
"bench_lowdisc", "bench_lowdisc.sce"; ..
"SRS vs QMC", "srs_vs_qmc.sce"; ..
"Scrambled Sobol (1)", "bench_sobolscrambling.sce"; ..
"Scrambled Sobol (2)", "bench_scrambled-sobol.sce"; ..
"Shifted Halton", "bench_shifted-halton.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
