// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
//
// Generate the library help
helpdir = cwd;
funarray = [
  "intprb_getsetup"
  "intprb_getexpect"
  "intprb_getfcn"
  "intprb_getname"
  "intprb_getproblems"
  "intprb_getvariance"
  "intprb_getvarprbs"
  ];
macrosdir = helpdir +"../../macros";
demosdir = [];
modulename = "intprbs";
helptbx_helpupdate ( funarray , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the support functions help
helpdir = fullfile(cwd,"supportfunctions");
funmat = [
  "intprb_contour"
  "intprb_shragerandom"
  "intprb_tuplenext"
  "intprb_muvstepstart"
  "intprb_muvstepupdate"
  "intprb_muvstepstop"
  "intprb_muvblockupdate"
  "intprb_getpath"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "intprbs";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the support functions help
helpdir = fullfile(cwd,"integration");
funmat = [
  "intprb_montecarlo"
  "intprb_crudemc"
  "intprb_getcrudemc"
  "intprb_crudeld"
  "intprb_getcrudeld"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "intprbs";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


