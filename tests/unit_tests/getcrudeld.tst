// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////

// With default parameters
nprob = 1;
[n,p] = intprb_getsetup(nprob);
callf = 1.e4;
integr = intprb_getcrudeld ( nprob , n , p , callf );
assert_checkalmostequal ( integr , 0 , [] , 1.e-4 );

// Customize the sequence
nprob = 1;
[n,p] = intprb_getsetup(nprob);
callf = 1.e4;
//ldseq = "haltonf";
ldseq = "halton";
integr = intprb_getcrudeld ( nprob , n , p , callf , ldseq );
assert_checkalmostequal ( integr , 0 , [] , 1.e-2 );


