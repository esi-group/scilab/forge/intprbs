// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

function r = sumfunction (m,n,x)
  // sum of n variables
  // Expectation = 0, Variance = 1
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F1.

    // The function value
    // The expectation of f (before scaling)
    v = n/12
    // The variance of f (before scaling)
    e = n/2
    r = sqrt(1/v) * (sum(x,"c") - e)
endfunction
//
//////////////////////////////////////////
//
// Test with default settings
grand('setgen',"mt");
grand("setsd",123456);
dim_num = 10;
// [ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , [] , [] , [] , [] , %t )
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-2 );
assert_checkequal ( evalf > 1.e4 , %t );
assert_checkequal ( iter==5 , %t );
//
// Test with customized maximum number of evaluations
grand("setsd",123456);
dim_num = 10;
maxevalf = 1.e4;
minevalf = 1.e3;
evalf = [maxevalf minevalf];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-1 );
assert_checkequal ( evalf == maxevalf , %t );
//
// Test verbose
grand("setsd",123456);
dim_num = 10;
bounds = [];
evalf = [];
tol = [];
randgen = [];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf , bounds , tol , randgen , %t );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-2 );
assert_checkequal ( evalf > 1.e4 , %t );
assert_checkequal ( iter==5 , %t );
//
// Test tolrel
grand("setsd",123456);
dim_num = 10;
bounds = [];
evalf = [1.e5 1.e3];
tol = [1.e-3 1.e-2];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf , bounds , tol );
assert_checkalmostequal ( integr , 0 , [] , 1.e-2 );
assert_checkequal ( accur < 1.e-2 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-2 );
assert_checkequal ( evalf > 1.e4 , %t );
assert_checkequal ( iter> 10 , %t );
//
// Test tolabs
grand("setsd",123456);
dim_num = 10;
bounds = [];
evalf = [1.e5 1.e3];
tol = [1.e-2 1.e-3];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf , bounds , tol );
assert_checkalmostequal ( integr , 0 , [] , 1.e-2 );
assert_checkequal ( accur < 1.e-2 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-2 );
assert_checkequal ( evalf > 1.e4 , %t );
assert_checkequal ( iter > 10 , %t );
// TODO : randgen.
//
// Test with customized bounds
// integral_10^30 sqrt(x)  = 88.46266043324404
// E(f) = integral_10^30 sqrt(x)  * 1/20 = 4.423133021662201
// V(f) = integral_10^30 (sqrt(x) - 4.423133021662201 )^2 * 1/20 = 0.4358942726814049
function r = sqrtfunction (m,n,x)
  r = sqrt(x)
endfunction
grand("setsd",123456);
dim_num = 1;
evalf = [];
bounds = [10 30];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sqrtfunction , dim_num , evalf , bounds );
assert_checkalmostequal ( integr , 88.462660433244027 , 1.e-2 );
assert_checkequal ( accur < 1 , %t );
assert_checkalmostequal ( varf , 0.4358942726814049 , 1.e-2 );
assert_checkequal ( evalf > 1.e3 , %t );
assert_checkequal ( iter > 1 , %t );

//
// Customize randgen
function u = shrand ( m , n )
  global shrageseed
  u = zeros(m,n)
  for k = 1 : m
    for i = 1 : n
      [ value , shrageseed ] = intprb_shragerandom ( shrageseed );
      u(k,i) = value
    end
  end
endfunction
global shrageseed;
shrageseed = 123456;
dim_num = 3;
evalf = [1000 1000];
bounds = [];
tol = [];
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf , bounds , tol , shrand );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-1 );
assert_checkequal ( shrageseed , 1554624259 );

//
// Customize randgen, which has additionnal parameters
function u = mygen ( m , n , seed , gentype )
  grand ( "setgen" , gentype )
  grand ( "setsd" , seed )
  u = grand ( m , n , "def" )
endfunction
dim_num = 3;
evalf = [1000 1000];
bounds = [];
tol = [];
randgen = list(mygen,123456,"urand");
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( sumfunction , dim_num , evalf , bounds , tol , randgen );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-1 );

//
// Customize the function :
// use a function which has additionnal parameters.
function r = sumfunction2 (m,n,x,e,v)
  r = sqrt(1/v) * (sum(x,"c") - e)
endfunction
dim_num = 3;
func = list(sumfunction2,dim_num/2,dim_num/12);
[ integr , accur , varf , evalf , iter ] = intprb_montecarlo ( func , dim_num );
assert_checkalmostequal ( integr , 0 , [] , 1.e-1 );
assert_checkequal ( accur < 1.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-1 );

