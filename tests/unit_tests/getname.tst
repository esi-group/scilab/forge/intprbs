// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// Get all short names
name = intprb_getname();
assert_checkequal ( typeof(name) , "string" );
assert_checkequal ( size(name,"c") , 1 );
assert_checkequal ( size(name,"r") > 1 , %t );

// Get name for Sum test case
name = intprb_getname(1);
assert_checkequal ( name , "sum" );
name = intprb_getname(1,%f);
assert_checkequal ( name , "sum" );
name = intprb_getname(1,%t);
assert_checkequal ( name , "Sum of X" );

// Check that all names can be retrieved
nprobmax = intprb_getproblems();
for nprob = 1 : nprobmax
  name = intprb_getname(nprob);
  assert_checkequal ( typeof(name) , "string" );
  assert_checkequal ( size(name) , [1 1] );
  name = intprb_getname(nprob,%t);
  assert_checkequal ( typeof(name) , "string" );
  assert_checkequal ( size(name) , [1 1] );
end

// See a case of error
nprobmax = intprb_getproblems();
cmd = "name = intprb_getname(-1)";
execstr(cmd,"errcatch");
[str,errcode] = lasterror();
expected = msprintf("intprb_getname: The input argument nprob is -1 but should be between 1 and %d.",nprobmax);
assert_checkequal ( str , expected );
//
// Get all names at once
nprobmax = intprb_getproblems();
name = intprb_getname(1:nprobmax);
assert_checkequal ( size(name) , [nprobmax 1] );

