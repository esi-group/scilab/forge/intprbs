// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


//
// Basic test case : two non-empty matrices xa and xb.
xa = (1 : 10)';
na = size(xa,"*");
xb = (11 : 20)';
nb = size(xb,"*");
mua = mean(xa);
mub = mean(xb);
va = variance(xa);
vb = variance(xb);
[ mu , var ] = intprb_muvblockupdate ( na , nb , mua , mub , va , vb );
x = [xa' xb']';
assert_checkalmostequal ( mean(x) , mu , %eps );
assert_checkalmostequal ( variance(x) , var , %eps );

//
// Practical test case : an empty xa with a non-empty xb.
// This is used when the update is applied for the first time.
xa = [];
na = 0;
xb = (11 : 20)';
nb = size(xb,"*");
mua = 0;
mub = mean(xb);
va = 0;
vb = variance(xb);
[ mu , var ] = intprb_muvblockupdate ( na , nb , mua , mub , va , vb );
x = [xa' xb']';
assert_checkalmostequal ( mean(x) , mu , %eps );
assert_checkalmostequal ( variance(x) , var , %eps );

//
// Ten loops to simulate a practical use.
// The vectors xb are generated on the fly and the 
// mean and variance is updated by block.
// The largest mean and variance computed is of size nb=5,
// instead of the total size n=50.
grand("setsd",123456);
n = 0;
mu = 0;
var = 0;
x = [];
nb = 5;
for k = 1 : 10
  xb = grand(nb,1,"def");
  mub = mean(xb);
  vb = variance(xb);
  [ mu , var ] = intprb_muvblockupdate ( n , nb , mu , mub , var , vb );
  x = [x' xb']';
  n = n + nb;
end
assert_checkalmostequal ( mean(x) , mu , %eps );
assert_checkalmostequal ( variance(x) , var , 10 * %eps );


