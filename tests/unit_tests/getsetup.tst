// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


// Get dimension for Sum test case
[n,p]=intprb_getsetup(1);
assert_checkequal ( n , 10 );
assert_checkequal ( p , [] );



