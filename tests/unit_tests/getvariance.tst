// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// Get variance for Sum test case
nprob = 1;
[n,p]=intprb_getsetup(nprob);
var = intprb_getvariance(n,p,nprob);
assert_checkequal ( var , 1 );


