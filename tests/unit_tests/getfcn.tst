// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


// Get function value at x for Sum test case
nprob = 1;
[ n , p ] = intprb_getsetup (nprob);
m = 5;
x = grand(m,n,"def");
f = intprb_getfcn(m,n,x,p,nprob);
assert_checkequal ( size(f) , [m 1] );


