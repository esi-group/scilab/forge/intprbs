// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

prb_n = intprb_getvarprbs();
assert_checkequal ( typeof(prb_n) , "constant" );
assert_checkequal ( size(prb_n,"c") , 1 );
assert_checkequal ( size(prb_n,"r") >= 1, %t );

