// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


// 
// Display one problem at a time
str = intprb_getproblems(1);
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// With verbose
str = intprb_getproblems(1,%t);
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// Without verbose
str = intprb_getproblems(1,%f);
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// Display all problems
nprobmax = intprb_getproblems();
assert_checkequal ( nprobmax > 1 , %t );
for nprob = 1 : nprobmax
  intprb_getproblems(nprob);
  mprintf("\n");
end

// Display all problems in one call
nprobmax = intprb_getproblems();
intprb_getproblems(1:nprobmax);

//
// Get all problems
nprobmax = intprb_getproblems();
str = intprb_getproblems(1:nprobmax,%f);
expected = [
"No.    file            n    Name"
"---    ----            -    ----"
"#  1.  SUM             (10) Sum of X"
"#  2.  SQSUM           (10) Sum of Squares"
"#  3.  SUMSQROOT       (10) Sum of Square Roots"
"#  4.  PRODONES        (10) Product of Signed Ones"
"#  5.  PRODEXP         (10) Product of Exponentials         "
"#  6.  PRODCUB         (10) Product of Cubes                "
"#  7.  PRODX           (10) Product of X                    "
"#  8.  SUMFIFJ         (10) Sum of fi fj                    "
"#  9.  SUMF1FJ         (10) Sum of f1 fj                    "
"# 10.  HELLEKALEK      (10) Hellekalek                      "
"# 11.  ROOSARNOLD1     (10) Roos and Arnold 1               "
"# 12.  ROOSARNOLD2     (10) Roos and Arnold 2               "
"# 13.  ROOSARNOLD3     (10) Roos and Arnold 3               "
"# 14.  RST1            (10) Radovic, Sobol, Tichy (aj=1)    "
"# 15.  RST2            (10) Radovic, Sobol, Tichy (aj=j)    "
"# 16.  RST3            (10) Radovic, Sobol, Tichy (aj=j^2)  "
"# 17.  SOBOLPROD       (10) Sobol Product                   "
"# 18.  OSCILL          (6)  Genz - Oscillatory"
"# 19.  PRPEAK          (6)  Genz - Product Peak"
"# 20.  CORPEAK         (6)  Genz - Corner Peak"
"# 21.  GAUSSIAN        (6)  Genz - Gaussian"
"# 22.  C0              (6)  Genz - C0"
"# 23.  DISCONT         (6)  Genz - Discontinuous"
];
expected = stripblanks(expected);
str = stripblanks ( str );
assert_checkequal ( str , expected );

