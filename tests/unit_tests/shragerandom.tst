// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



// 
// Test from :
//    "A More Portable Fortran Random Number Generator", Linus Schrage, ACM Transactions on Mathematical Software, Volume 5, Number 2, June 1979, pages 132-138.
seed = 1;
for i = 1 : 1000
  [ value , seed ] = intprb_shragerandom ( seed );
end
assert_checkequal ( seed , 522329230 );

// 
// Check basic properties
seed = 1;
for i = 1 : 100
  [ value , seed ] = intprb_shragerandom ( seed );
  assert_checkequal ( value > 0 , %t );
  assert_checkequal ( value < 1 , %t );
  assert_checkequal ( seed >=1 , %t );
  assert_checkequal ( seed <=2147483647 , %t );
end

