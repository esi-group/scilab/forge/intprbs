// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////

  //    n = 2, m1 = 1, m2 = 3
  //
  //    InPUT        OUTPUT
  //    -------      -------
  //    rnk   x      rnk   x
  //    ----  ---    -----  ---
  //    0     * *    1      1 1
  //    1     1 1    2      1 2
  //    2     1 2    3      1 3
  //    3     1 3    4      2 1
  //    4     2 1    5      2 2
  //    5     2 2    6      2 3
  //    6     2 3    7      3 1
  //    7     3 1    8      3 2
  //    8     3 2    9      3 3
  //    9     3 3    0      0 0


n = 2;
m1 = 1;
m2 = 3;
rnk = 0;
ic = [];
computed = [];
for i = 1 : 12
  [ rnk, ic ] = intprb_tuplenext ( m1, m2, n, rnk, ic );
  computed(i,1:n+1) = [rnk ic'];
end
expected = [
  1,1,1;  
  2,1,2;
  3,1,3;
  4,2,1;
  5,2,2;
  6,2,3;
  7,3,1;
  8,3,2;
  9,3,3;
  0,0,0;
  1,1,1;
  2,1,2
  ];
assert_checkequal ( computed , expected );

