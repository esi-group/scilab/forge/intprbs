// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r=intprb_hellekalek(m,n,x,p,option)

  // Product of gj, with g(z) nonlinear, parametrized by alpha.
  // We choose alpha = 1.
  //
  // References
  //   "The Dimension Distribution And Quadrature Test Functions"
  //   Art B. Owen, Stanford University, Statistica Sinica 13(2003), 1-17.
  //
  //   "On the assessment of random and quasi-random point sets",
  //   in "Random and quasi-random point sets", edited by Hellekalek andLarcher,
  //   49-108, Springer, New York

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_hellekalek",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    alpha = 1
    // The variance of function gj
    gamma2j = alpha^2 / ((2*alpha+1) * (alpha+1)^2)
    g = ( x.^alpha - 1/(alpha+1) ) / sqrt(gamma2j)
    r = prod(g,"c")
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Hellekalek"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

