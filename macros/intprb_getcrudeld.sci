// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function integr = intprb_getcrudeld ( varargin )
    //  Uses a crude Quasi Monte-Carlo to estimate the integral.
    //
    // Calling Sequence
    //   integr = intprb_getcrudeld ( nprob , n , p , callf )
    //   integr = intprb_getcrudeld ( nprob , n , p , callf , ldseq )
    //
    //  Parameters
    //   nprob: the problem number
    //   n: a 1-by-1 matrix of floating point integers, the spatial dimension.
    //   p: a floating point matrix, the parameters of the problem
    //   callf : a 1-by-1 matrix of floating point integers, the number of calls to the function (default = 1.e4)
    //   ldseq : a 1-by-1 matrix of strings, the name of the sequence (Default = "sobol"). The list of available sequences can be computed by lowdisc_methods().
    //   integr : a 1-by-1 matrix of doubles, the approximate value of the integral, the mean of the function.
    //   accur : a 1-by-1 matrix of doubles, the estimated error on the integral.
    //   varf : a 1-by-1 matrix of doubles, the approximate value of the variance of f.
    // 
    // Description
    //   Uses a crude Quasi Monte-Carlo to approximate the integral corresponding 
    //   to the nprob problem.
    //   The computation is based on a Low Discrepancy sequence.
    //   This function is a wrapper on intprb_crudeld.
    //
    // Examples
    // // Consider problem #1
    // nprob = 1;
    // [n,p] = intprb_getsetup(nprob);
    // callf = 1.e4;
    // // Get the exact result
    // e = intprb_getexpect(n,p,nprob)
    // // Use a Low Discrepancy sequence
    // integr = intprb_getcrudeld ( nprob , n , p , callf )
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    //  2010 - Michael Baudin
    //

    [lhs, rhs] = argn();
    apifun_checkrhs ( "intprb_getcrudeld" , rhs , 4 : 5 )
    apifun_checklhs ( "intprb_getcrudeld" , lhs , 0 : 1 )
    //
    nprob = varargin(1)
    n = varargin(2)
    p = varargin(3)
    callf = varargin(4)
    ldseq = apifun_argindefault ( varargin , 5 , "sobol" )
    //
    // Check arguments
    apifun_checktype ( "intprb_getcrudeld" , nprob , "nprob" , 1 , "constant" )
    apifun_checktype ( "intprb_getcrudeld" , n ,     "n" ,     2 , "constant" )
    apifun_checktype ( "intprb_getcrudeld" , p ,     "p" ,     3 , "constant" )
    apifun_checktype ( "intprb_getcrudeld" , callf , "callf" , 4 , "constant" )
    apifun_checktype ( "intprb_getcrudeld" , ldseq , "ldseq" , 5 , "string" )
    //
    apifun_checkscalar ( "intprb_getcrudeld" , nprob , "nprob" , 1 )
    apifun_checkscalar ( "intprb_getcrudeld" , n ,     "n" ,     2 )
    // Do not check the size of p : let the particular function manage its own data.
    apifun_checkscalar ( "intprb_getcrudeld" , callf , "callf" , 4 )
    apifun_checkscalar ( "intprb_getcrudeld" , ldseq , "ldseq" , 5 )
    //
    // Define a wrapper over the intprb function
    body = [
    "y = intprb_getfcn ( callf , n , x , p , nprob );"
    ];
    prot=funcprot();
    funcprot(0);
    deff("y = func ( callf , n , x , p , nprob )",body);
    funcprot(prot);
    //
    integr = intprb_crudeld ( func , n , ldseq , callf )
endfunction


