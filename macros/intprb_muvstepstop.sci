// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function [ n , mu , v ] = intprb_muvstepstop ( n , mu , v )
  // Finish the computation of the step-by-step (mean,variance) computation.
  //
  // Calling Sequence
  //   [ n , mu , v ] = intprb_muvstepstop ( n , mu , v )
  //
  // Parameters
  //   n : a 1-by-1 matrix of floating point integers, the number of values
  //   mu : a 1-by-1 matrix of doubles, the mean
  //   v : a 1-by-1 matrix of doubles, the sample variance, as the sum of squares divided by with n-1 
  //
  // Description
  // This routine is used in an algorithm which uses the step-by-step 
  // variance computation.
  // It is designed to be used with intprb_muvstepupdate and intprb_muvstepstop.
  // The sum of squares stored in v is divided by n - 1.
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  // Donald E. Knuth (1998). The Art of Computer Programming, volume 2: Seminumerical Algorithms, 3rd edn., p. 232. Boston: Addison-Wesley.
  // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  
  v = v / (n - 1)
endfunction

