// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ mu , v ] = intprb_muvblockupdate ( na , nb , mua , mub , va , vb )
  // Update the mean and the variance of x, given the mean and the value of disjoint parts of x.
  //
  // Calling Sequence
  //   [ mu , v ] = intprb_muvblockupdate ( na , nb , mua , mub , va , vb )
  //
  // Parameters
  //   na : a 1-by-1 matrix of floating point integers, the number of values in xa
  //   nb : a 1-by-1 matrix of floating point integers, the number of values in xb
  //   mua : a 1-by-1 matrix of doubles, the mean of the matrix xa
  //   mub : a 1-by-1 matrix of doubles, the mean of the matrix xb
  //   va : a 1-by-1 matrix of doubles, the variance of the matrix xa, as the sum of squares divided by with na-1 
  //   vb : a 1-by-1 matrix of doubles, the variance of the matrix xb, as the sum of squares divided by with nb-1 
  //   mu : a 1-by-1 matrix of doubles, the mean of x, the union of xa and xb
  //   v : a 1-by-1 matrix of doubles, the sample variance, as a sum of squares divided by with n-1 
  //
  // Description
  // The algorithm uses an updating scheme of the mean and the variance
  // in order to update them block by block.
  // It uses a decomposition of the array x into the two arrays xa and xb, 
  // with their associated means mua and mub and their variances va and vb.
  // The purpose of the computation is to show that the mean and the 
  // variance can be computed without accessing to the whole array x at once.
  // The routine has no direct use, since the vectorization used in the 
  // built-in variance function is faster.
  // But it can be used as a template for other uses.
  //
  // Examples
  // // Compare our algorithm with the functions (mean,variance)
  // na = 10
  // nb = 10
  // xa = (1 : na)'
  // xb = (na+1 : 20)'
  // mua = mean(xa)
  // mub = mean(xb)
  // va = variance(xa)
  // vb = variance(xb)
  // [ mu , v ] = intprb_muvblockupdate ( na , nb , mua , mub , va , vb )
  // x = [xa' xb']'
  // disp ( [ mean(x) mu ] )
  // disp ( [ variance(x) v ] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  
  M2a = va * (na - 1)
  M2b = vb * (nb - 1)
  delta = mub - mua
  mu = (na * mua + nb * mub) / (na + nb)
  M2 = M2a + M2b + delta^2 * na * nb / (na + nb)
  v = M2 / (na + nb - 1)
endfunction

