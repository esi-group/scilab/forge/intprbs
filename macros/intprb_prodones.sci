// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r = intprb_prodones (m,n,x,p,option)

  // Product of fj, where fj = +/-1
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F4.

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_prodones",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    v = 1
    e = 0
    f = ones(m,n)
    klow = find(x<0.5)
    f(klow) = -1
    r = prod(f,"c")
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Product of Signed Ones"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

