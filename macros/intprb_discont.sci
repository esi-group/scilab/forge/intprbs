// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
// Copyright (C) 1987 - Alan Genz
//
// This file must be used under the terms of the GNU LGPL license.

function r = intprb_discont (m,n,x,p,option)
  
  // Discontinuous function by Genz. 
  // Actually, a multidimensional modification of the Discontinuous function.
  // The condition "x1 > u1 or x2 > u2" is replaced by "or(x>u)".
  //
  // This problem is difficult, because the region where the function is non-zero
  // is small. 
  // The early iterations of a Monte-Carlo
  // are all associated to a function evaluation equal to zero, giving 
  // an average equal to zero. 
  // Only after many points are generated,
  // a random point is located where the function is non-zero, 
  // hence producing a much better evaluation of the integral.
  // Since this integral is scaled so that it is equal to zero, the 
  // variance associated with the early iterations is zero.
  // Reference
  //    Alan Genz,
  //    A Package for Testing Multiple Integration Subroutines,
  //    in Numerical Integration:
  //    Recent Developments, Software and Applications,
  //    edited by Patrick Keast, Graeme Fairweather,
  //    D Reidel, 1987, pages 337-340,
  //    LC: QA299.3.N38.
  
  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_discont",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    p_alpha = p(1:n)
    p_beta = p(n+1:2*n)
    //
    // The expectation
    terms = ( exp ( p_alpha .* p_beta ) - 1.0 ) ./ p_alpha
    e = prod ( terms )
    //
    malpha = ones(m,1) * p_alpha'
    mbeta = ones(m,1) * p_beta'
    r = zeros(m,1)
    k = find ( and ( mbeta > x , "c" ) )
    r(k) = x(k,:) * p_alpha
    r = r - e
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = []
  case 4
    // The default number of variables
    r = 6
  case 5
    // The full name of the test case
    r = "Genz - Discontinuous"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    //p_alpha = grand(n,1,"def") would be faster, but not as close to the original source code
    seed = 123456
    p_alpha = zeros(n,1)
    p_beta = zeros(n,1)
    for i = 1 : n
      [ value, seed ] = intprb_shragerandom ( seed )
      p_alpha(i) = value
      // Set beta as in Genz's testpack.f
      if ( i <= 2 ) then
        [ value, seed ] = intprb_shragerandom ( seed )
        p_beta(i) = value
      else
        p_beta(i) = 1
      end
    end
    // Scale a as recommended in Genz in testpack.f : s^2/100
    suma = sum(p_alpha)
    scale = n^2 / 100
    p_alpha = p_alpha / suma / scale
    r(1:n) = p_alpha
    r(n+1:2*n) = p_beta
  end
endfunction

