// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r=intprb_sobolprod(m,n,x,p,option)

  // Product of gj, with g(xj) = (1+2xj)/(j + 1).
  // The importance of the variable xj decreases when the j increases.
  // For this function, we expect a good performance of quasi Monte Carlo.
  //
  // References
  //
  // "A Primer for the Monte Carlo Method"
  // Ilya Sobol, 1994, CRC Press
  //
  //   "The Dimension Distribution And Quadrature Test Functions"
  //   Art B. Owen, Stanford University, Statistica Sinica 13(2003), 1-17.
  //

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_sobolprod",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    // The expectation of f (before scaling)
    e = 1
    // The variance of gj
    v1n = (1:n)'
    gamma2 = 1 ./ (3*(v1n + 1).^2)
    // The variance of f (before scaling)
    v = prod(1 + gamma2) - 1
    // p : the m x n matrix of integers 1,2,...,n
    p = ones(m,1) * v1n'
    g = (p + 2*x)./(p + 1)
    r = sqrt(1/v)*(prod(g,"c")-e)
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Sobol Product"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

