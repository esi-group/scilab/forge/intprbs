// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ n , mu , v ] = intprb_muvstepupdate ( n , mu , v , x )
  // Update values for the step-by-step (mean,variance) computation.
  //
  // Calling Sequence
  //   [ n , mu , v ] = intprb_muvstepupdate ( n , mu , v , x )
  //
  // Parameters
  //   n : a 1-by-1 matrix of floating point integers, the number of values
  //   mu : a 1-by-1 matrix of doubles, the mean
  //   v : a 1-by-1 matrix of doubles, the sample variance, as the sum of squares divided by with n-1 
  //   x : a 1-by-1 matrix of doubles, the new value to be taken into account
  //
  // Description
  // This routine is used in an algorithm which uses the step-by-step 
  // variance computation.
  // It is designed to be used with intprb_muvstepupdate and intprb_muvstepstop.
  // During the update, the variable v holds the sum of squares, not 
  // the sample variance.
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  // Donald E. Knuth (1998). The Art of Computer Programming, volume 2: Seminumerical Algorithms, 3rd edn., p. 232. Boston: Addison-Wesley.
  // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  
  n = n + 1
  delta = x - mu
  mu = mu + delta/n
  // This expression uses the new value of mean
  v = v + (x - mu) * delta
endfunction

