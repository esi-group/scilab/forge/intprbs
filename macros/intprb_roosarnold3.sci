// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r=intprb_roosarnold3(m,n,x,p,option)

  // Product of gj, with g(z) = (pi/2)sin(pi*xj).
  // This is a difficult fonction to integrate numerically.
  // The dimension distribution grows linearly with s.
  //
  // References
  //
  //   "The Dimension Distribution And Quadrature Test Functions"
  //   Art B. Owen, Stanford University, Statistica Sinica 13(2003), 1-17.
  //
  //   "Methods of Numerical Integration", Second Edition,
  //   Philip J. Davis and Philip Rabinowitz, 1984, 
  //   p. 405-406
  //
  //   "Numerische Experimente Zur Mehrdimensionalen Quadratur", 
  //   Osterreich. Akad. Wiss. Math.-Natur., KI. S.-B. II, 172, 1963, 271-286, 
  //   P. Roos and L. Arnold
  //

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_roosarnold3",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    // The expectation of f (before scaling)
    e = 1
    // The variance of f (before scaling)
    v = (%pi^2/8)^n - 1
    g = (%pi/2)*sin(%pi*x)
    r = sqrt(1/v)*(prod(g,"c")-e)
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Roos and Arnold 3"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

