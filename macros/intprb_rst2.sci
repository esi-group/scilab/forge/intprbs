// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r=intprb_rst2(m,n,x,p,option)

  // Product of gj, with g(xj) = (abs(4xj - 2)+aj)/(1+aj).
  // We consider aj = j.
  // The importance of the variable j decreases when the j increases.
  //
  // References
  //
  // "Quasi-Monte Carlo Methods for Numerical Integration: Comparison of Different Low Discrepancy Sequences"
  // Igor Radovic and Ilya M. Sobol and Robert F. Tichy
  // Monte Carlo Methods and Applications, 1996, Volume 2, Issue 1, p. 1-14
  //
  //   "The Dimension Distribution And Quadrature Test Functions"
  //   Art B. Owen, Stanford University, Statistica Sinica 13(2003), 1-17.
  //

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_rst2",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    // The expectation of f (before scaling)
    e = 1
    // The variance of f (before scaling)
    a = (1:n)'
    gamma2j = 1 ./ (3*(1+a).^2)
    v = prod(1 + gamma2j) - 1
    g = (abs(4*x-2) + ones(m,1) * a')./(1+ones(m,1) * a')
    r = sqrt(1/v)*(prod(g,"c")-e)
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Radovic, Sobol, Tichy (aj=j)"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

