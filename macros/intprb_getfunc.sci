// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function r = intprb_getfunc ( m,n,x,p,nprob,option)
  // Returns a given property of one problem.
  //
  // Calling Sequence
  //   r=intprb_getfunc(m,n,x,p,nprob,option)
  //
  // Parameters
  //   m: a floating point integer, the number of experiments
  //   n: a floating point integer, the dimension of the space
  //   x: a m x n matrix of doubles, the point where to compute f.
  //   p: a matrix of doubles, the parameters of the problem
  //   nprob: the problem number
  //   option: a floating point integer, the output to return. The option integer can be option = 1, 2, 3, 4, 5, 6 or 7. If option=1, returns the function value f (x and m are used), option=2 returns the expectation e (x and m are not used), option= 3 returns the variance v (x and m are not used), option= 4 returns the default number of variables (x, n, m are not used), option= 5 returns the name of the problem (x, n, m are not used), option= 6 returns true/false if the problem has variable dimension (x, n, m are not used), option= 7 returns the parameters of the problem (x, m are not used). 
  //   r: if option = 1, a m x 1 matrix of doubles, the vector [f(x(1,:)), f(x(2,:)), ... f(x(m,:))]^T. If option = 2, a 1-by-1 matrix of doubles, the expectation. If option = 3, a 1-by-1 matrix of doubles, the variance. If option = 4, a 1-by-1 matrix of floating point integers, the default number of variables. If option = 5, a 1-by-1 matrix of strings, the full name of the test function. If option = 6, a 1-by-1 matrix of booleans, true if the problem has variable dimension. If option = 7, a matrix of doubles, the parameters of the problem (its size depends on the problem).
  //
  // Description
  // Selects the appropriate test function based on nprob.
  //
  // Examples
  //
  //   // Get the default number of variables for Sum test case
  //   nprob = 1
  //   option = 4
  //   m = []
  //   x = []
  //   p = []
  //   n = intprb_getfunc(m,[],x,p,nprob,option)
  //
  //   // Get the parameters for Sum test case
  //   nprob = 1
  //   option = 7
  //   m = []
  //   x = []
  //   n = intprb_getfunc(m,[],x,[],nprob,option)
  //   p = intprb_getfunc(m,n,x,[],nprob,option)
  //
  //   // Get the full test name for Sum test case
  //   nprob = 1
  //   option = 5
  //   m = []
  //   n = []
  //   x = []
  //   p = []
  //   name = intprb_getfunc(m,n,x,p,nprob,option)
  //
  //   // Get the expectation for Sum test case
  //   nprob = 1
  //   option = 2
  //   m = []
  //   x = []
  //   n = intprb_getfunc(m,[],x,[],nprob,option)
  //   p = intprb_getfunc(m,n,x,[],nprob,option)
  //   e = intprb_getfunc(m,n,x,p,nprob,option)
  //
  //   // Get the variance for Sum test case
  //   nprob = 1
  //   option = 3
  //   m = []
  //   x = []
  //   n = intprb_getfunc(m,[],x,[],nprob,option)
  //   p = intprb_getfunc(m,n,x,[],nprob,option)
  //   v = intprb_getfunc(m,n,x,p,nprob,option)
  //
  //   // Get f for Sum test case
  //   nprob = 1
  //   m = []
  //   x = []
  //   n = intprb_getfunc(m,[],x,[],nprob,4)
  //   p = intprb_getfunc(m,n,x,[],nprob,7)
  //   m = 1
  //   x = ones(m,n)
  //   option = 1
  //   f = intprb_getfunc(n,m,x,p,nprob,option)
  //
  //   // See if Sum test case is a variable-dimension problem
  //   nprob = 1
  //   option = 6
  //   m = []
  //   n = []
  //   x = []
  //   p = []
  //   vardim = intprb_getfunc(m,n,x,p,nprob,6)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getfunc" , rhs , 6 )
  apifun_checklhs ( "intprb_getfunc" , lhs , 0 : 1 )
  //
  //
  // Check input arguments
  if ( m <> [] ) then
  apifun_checktype   ( "intprb_getfunc" , m ,       "m" ,      1 , "constant" )
  apifun_checkscalar ( "intprb_getfunc" , m ,       "m" ,      1 )
  end
  if ( n <> [] ) then
  apifun_checktype   ( "intprb_getfunc" , n ,       "n" ,      2 , "constant" )
  apifun_checkscalar ( "intprb_getfunc" , n ,       "n" ,      2 )
  end
  if ( x <> [] ) then
  apifun_checktype   ( "intprb_getfunc" , x ,       "x" ,      3 , "constant" )
  apifun_checkdims   ( "intprb_getfunc" , x ,       "x" ,      3 , [m n] )
  end
  if ( p <> [] ) then
  apifun_checktype   ( "intprb_getfunc" , p ,       "p" ,      4 , "constant" )
  // Do not check the size of p : let the function manage it.
  end
  apifun_checktype   ( "intprb_getfunc" , nprob ,   "nprob" ,  5 , "constant" )
  apifun_checkscalar ( "intprb_getfunc" , nprob ,   "nprob" ,  5 )
  apifun_checktype   ( "intprb_getfunc" , option ,  "option" , 6 , "constant" )
  apifun_checkscalar ( "intprb_getfunc" , option ,  "option" , 6 )
  apifun_checkoption ( "intprb_getfunc" , option ,  "option" , 6 , 1:7 )
  //
  name = intprb_getname(nprob)
  execstr("r = intprb_" + name + "(m,n,x,p,option)")
endfunction

