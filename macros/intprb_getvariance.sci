// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function v = intprb_getvariance ( n , p , nprob )
  // Compute the variance.
  //
  // Calling Sequence
  //   v = intprb_getvariance ( n , p , nprob )
  //
  // Parameters
  //   n: a 1-by-1 matrix of doubles, integer value, the dimension of the space
  //   p: a matrix of doubles, the parameters of the problem
  //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
  //   v: a 1-by-1 matrix of doubles, the variance
  //
  // Description
  // This function returns the variance associated with the 
  // problem <literal>nprob</literal>.
  //
  // Internally, it is an interface function which calls the function getfunc (which selects  
  // appropriate test fuction based on nprob).
  // If the variance is known, the returned value of v is unity.
  // If the variance is unknown, the returned value of v is the empty matrix.
  // For Genz's functions, the variance is unknown.
  //
  // See the "Integration Problems User's Manual" available in the doc 
  // directory of this toolbox for a detailed description of this function.
  //
  // Examples
  //   // Get v for Sum test case
  //   nprob = 1
  //   [n,p] = intprb_getsetup(nprob)
  //   v=intprb_getvariance(n,p,nprob)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  //   "Integration Problems User's Manual", Michael Baudin, 2010

  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getvariance" , rhs , 3 )
  apifun_checklhs ( "intprb_getvariance" , lhs , 0 : 1 )
  //
  //
  // Check input arguments
  apifun_checktype   ( "intprb_getvariance" , n ,        "n" ,    1 , "constant" )
  apifun_checkscalar ( "intprb_getvariance" , n ,        "n" ,    1 )
  // Do not check p : let the test functions manage it as they want.
  apifun_checktype   ( "intprb_getvariance" , nprob ,    "nprob" ,    3 , "constant" )
  apifun_checkscalar ( "intprb_getvariance" , nprob ,    "nprob" ,    3 )
  //
  v = intprb_getfunc([],n,[],p,nprob,3)
endfunction

