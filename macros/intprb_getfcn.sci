// Copyright (C) 2011,2013 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function f = intprb_getfcn (m,n,x,p,nprob)
  // Returns the function value.
  //
  // Calling Sequence
  //   f=intprb_getfcn(m,n,x,p,nprob)
  //
  // Parameters
  //   m: a 1-by-1 matrix of doubles, integer value, the number of experiments
  //   n: a 1-by-1 matrix of doubles, integer value, the dimension of the space
  //   x: a m-by-n matrix of doubles, the point where to compute f.
  //   p: a matrix of doubles, the parameters of the problem
  //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
  //   f: a m-by-1 matrix of doubles, the function value
  //
  // Description
  // This function computes the function value at the point <literal>x</literal>.
  // 
  // Internally, it is an interface function which calls the function 
  // <literal>getfunc</literal>, which selects  
  // appropriate test fuction based on <literal>nprob</literal>.
  //
  // See the "Integration Problems User's Manual" available in the doc 
  // directory of this toolbox for a detailed description of this function.
  //
  // Examples
  //   // Get function value at [1 1 1] for Sum test case
  //   nprob = 1
  //   [n,p] = intprb_getsetup(nprob)
  //   x = ones(1,n)
  //   m = 1
  //   f = intprb_getfcn(m,n,x,p,nprob)
  //
  //   // Get function value for randomized experiments for Sum test case
  //   nprob = 1
  //   [n,p] = intprb_getsetup(nprob)
  //   m = 10
  //   x = grand(m,n,"def")
  //   f = intprb_getfcn(m,n,x,p,nprob)
  //   // Print summary statistics 
  //   [min(f) max(f) mean(f) st_deviation(f)]
  //
  //   // Perform a Monte-Carlo on Sum
  //   nprob = 1;
  //   [n,p] = intprb_getsetup(nprob)
  //   e = intprb_getexpect(n,p,nprob)
  //   v = intprb_getvariance(n,p,nprob)
  //   for m = logspace(3,5,3)
  //     x = grand(m,n,"def");
  //     f = intprb_getfcn(m,n,x,p,nprob);
  //     meanm = mean(f);
  //     varm = variance(f);
  //     mprintf("%10d %+7.5f %.7f %.7f\n",m,meanm,varm,varm/m);
  //   end
  //
  // Authors
  // Copyright (C) 2011,2013 - Michael Baudin
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // Bibliography
  //   "Integration Problems User's Manual", Michael Baudin, 2010

  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getfcn" , rhs , 5 )
  apifun_checklhs ( "intprb_getfcn" , lhs , 0 : 1 )
  //
  apifun_checktype   ( "intprb_getfcn" , m ,       "m" ,      1 , "constant" )
  apifun_checkscalar ( "intprb_getfcn" , m ,       "m" ,      1 )
  apifun_checktype   ( "intprb_getfcn" , n ,       "n" ,      2 , "constant" )
  apifun_checkscalar ( "intprb_getfcn" , n ,       "n" ,      2 )
  apifun_checktype   ( "intprb_getfcn" , x ,       "x" ,      3 , "constant" )
  apifun_checkdims   ( "intprb_getfcn" , x ,       "x" ,      3 , [m n] )
  apifun_checktype   ( "intprb_getfcn" , p ,       "p" ,      4 , "constant" )
  // Do not check the size of p : let the function manage it.
  apifun_checktype   ( "intprb_getfcn" , nprob ,   "nprob" ,  5 , "constant" )
  apifun_checkscalar ( "intprb_getfcn" , nprob ,   "nprob" ,  5 )
  //
  f = intprb_getfunc(m,n,x,p,nprob,1)
endfunction

