// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [n,p] = intprb_getsetup (nprob)
  // Returns the parameters of the problem.
  //
  // Calling Sequence
  //   [n,p] = intprb_getsetup(nprob)
  //
  // Parameters
  //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
  //   n: a 1-by-1 matrix of doubles, integer value, the dimension of the space
  //   p: a matrix of doubles, the parameters of the problem
  //
  // Description
  // This function returns the dimension n and the parameters p of the problem.
  // A problem may have no parameter : in this case, we return p=[].
  // If a problem has parameters, then p is a non empty matrix filled 
  // with values. The number of values depends on the problem.
  // In fact, the variable p is empty for most problems: currently,
  // only the problems associated with Genz functions store non empty
  // parameter p.
  //
  // Examples
  //   // Get the parameters for Sum test case
  //   nprob = 1
  //   [n,p] = intprb_getsetup(nprob)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2011 - Michael Baudin


  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getsetup" , rhs , 1 )
  apifun_checklhs ( "intprb_getsetup" , lhs , 0 : 2 )
  //
  //
  // Check input arguments
  apifun_checktype   ( "intprb_getsetup" , nprob ,    "nprob" ,    1 , "constant" )
  apifun_checkscalar ( "intprb_getsetup" , nprob ,    "nprob" ,    1 )
  //
  n = intprb_getfunc([],[],[],[],nprob,4)
  p = intprb_getfunc([],n,[],[],nprob,7)
endfunction

