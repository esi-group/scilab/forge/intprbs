// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r = intprb_sumf1fj (m,n,x,p,option)

  // Sum of f1 fj, where fj is a cubic polynomial
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F9.
  // I was forced to modify the function definition :
  // the original paper probably contains an error with the function definition.

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_sumf1fj",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    s = sqrt(1/(n-1))
    f = 27.20917094*x.^3 - 36.19250850*x.^2 + 8.983337562*x + 0.7702079855
    //
    g = f(:,1) .* sum(f(:,2:n),"c")
    //
    r = s * g
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Sum of f1 fj"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction


