// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ n , mu , v ] = intprb_muvstepstart ( )
  // Returns the starting values for the step-by-step (mean,variance) computation.
  //
  // Calling Sequence
  //   [ n , mu , v ] = intprb_muvstepstart ( )
  //
  // Parameters
  //   n : a 1-by-1 matrix of floating point integersthe starting value of the number of values
  //   mu : a 1-by-1 matrix of doubles, the starting value of the mean
  //   v : a 1-by-1 matrix of doubles, the starting value of the sample variance, as the sum of squares divided by with n-1 
  //
  // Description
  // This routine is used in an algorithm which uses the step-by-step 
  // variance computation.
  // It is designed to be used with intprb_muvstepupdate and intprb_muvstepstop.
  // This function is useful in Monte-Carlo algorithms which 
  // do not keep track of all the function values, but only
  // keep the last function value and update the mean and the variance 
  // dynamically.
  //
  // Examples
  // // Compare our algorithm with the functions (mean,variance)
  //   x = 1 : 10
  //   [ n , mu , v ] = intprb_muvstepstart ( );
  //   for i = 1 : 10
  //     [ n , mu , v ] = intprb_muvstepupdate ( n , mu , v , x(i) );
  //   end
  //   [ n , mu , v ] = intprb_muvstepstop ( n , mu , v );
  //   disp( [mean(x) mu ] )
  //   disp( [variance(x) v ] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  // Donald E. Knuth (1998). The Art of Computer Programming, volume 2: Seminumerical Algorithms, 3rd edn., p. 232. Boston: Addison-Wesley.
  // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance

  n = 0
  mu = 0
  v = 0
endfunction

