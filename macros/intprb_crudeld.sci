// Copyright (C) 2011-2013 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function integr = intprb_crudeld ( varargin )
    // Estimates a multidimensional integral using a Low Discrepancy sequence.
    //
    // Calling Sequence
    //   integr = intprb_crudeld ( func , n )
    //   integr = intprb_crudeld ( func , n , ldseq )
    //   integr = intprb_crudeld ( func , n , ldseq , callf )
    //   integr = intprb_crudeld ( func , n , ldseq , callf , bounds )
    //
    // Parameters
    //   func : a function or a list, the function to be evaluated. If func is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end. See below for details.
    //   n: a 1-by-1 matrix of floating point integers, the spatial dimension.
    //   ldseq : a 1-by-1 matrix of strings, a function or a list, the name of the sequence (Default = "sobol"). The list of available sequences can be computed by lowdisc_methods(). The ldseq variable might also be a function, or a list. See below for details.
    //   callf : a 1-by-1 matrix of floating point integers, the number of calls to the function (default = 1.e3)
    //   bounds : a n-by-2 matrix of doubles, where lowb = bounds(:,1) = lower bound and uppb = bounds(:,2) = upper bound of integration (default = [zeros(n,1) ones(n,1)])
    //   integr : a 1-by-1 matrix of doubles, the approximate value of the integral, the mean of the function.
    //
    // Description
    // The algorithm uses a crude Low Discrepancy sequence to estimate the integral.
    //
    // The function <literal>func</literal> should have header
    //
    // <screen>
    // y = func( m , n , x ) 
    // </screen>
    //
    // where 
    // n is a floating point representing the spatial dimension, 
    // m is a floating point integer representing the number of experiments to perform and
    // x is a m-by-n matrix of doubles.
    // 
    // It might happen that the function requires additionnal 
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument func can also be a list, where the first item is 
    // a function f. 
    // The function f must have the header 
    //
    // <screen>
    // y = f ( m , n , x , a1 , a2 , ... )
    // </screen>
    //
    // In this case the func variable should hold the list (f,a1,a2,...) and the 
    // input arguments a1, a2, will be automatically be appended at the 
    // end of the calling sequence of rg.
    //
    // If an optional input argument is equal to the empty matrix, it 
    // is set to its default value.
    //
    // The low discrepancy generator ldseq may also be a function, with header 
    //
    // <screen>
    // u = ldseq ( m , n ) 
    // </screen>
    //
    // where m is a 1-by-1 matrix of floating point integers, the number of experiments to be performed
    // and u is a m x n matrix of doubles in the interval [0,1].
    //
    // It might happen that the low discrepancy generator requires additionnal 
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument ldseq can also be a list, where the first item is 
    // a function rg. 
    // The function rg must have the header :
    //
    // <screen>
    // u = rg ( m , n , a1 , a2 , ... )
    // </screen>
    //
    // In this case the ldseq variable should hold the list (rg,a1,a2,...) and the 
    // input arguments a1, a2, will be automatically be appended at the 
    // end of the calling sequence of rg.
    //
    // Examples
    // // A basic example
    // function r = sumfun (m,n,x)
    //   // sum of n variables
    //   // Expectation = 0, Variance = 1
    //   v = n/12
    //   e = n/2
    //   r = sqrt(1/v) * (sum(x,"c") - e)
    // endfunction
    // n = 10;
    // // The exact answer is 0.
    // integr = intprb_crudeld ( sumfun , n )
    //
    // // Customize the Low Discrepancy sequence
    // dim_num = 10;
    // ldseq = "haltonf";
    // // The exact answer is 0.
    // integr = intprb_crudeld ( sumfun , dim_num , ldseq )
    //
    // // Customize the bounds
    // // integral_10^30 sqrt(x)  = 88.46266043324404
    // // E(f) = integral_10^30 sqrt(x)  * 1/20 
    // //      = 4.423133021662201
    // // V(f) = integral_10^30 (sqrt(x) - E(f) )^2 * 1/20 
    //         = 0.4358942726814049
    // function r = sqrtfun (m,n,x)
    //   r = sqrt(x)
    // endfunction
    // n = 1;
    // callf = [];
    // ldseq = [];
    // bounds = [10 30];
    // // The exact answer is 88.46266043324404.
    // integr=intprb_crudeld(sqrtfun,n,callf,ldseq,bounds)
    //
    // // Customize the low Discrepancy sequence.
    // // Uses our own generator (here, a basic random number generator).
    // // Caution : this is a wrong use of the function, which expects
    // // a Low Discrepancy sequence.
    // // Customize ldseq with a home-made Halton sequence
    // // restricted to dimension 10.
    // function result = vdc ( i , basis )
    //   current = i
    //   ib = 1.0 / basis
    //   result = 0.0
    //   while (current>0)
    //     digit = modulo ( current , basis )
    //     current = int ( current / basis )
    //     result = result + digit * ib
    //     ib = ib / basis
    //   end
    // endfunction
    // function next = haltonseq ( m , n )
    //   next = zeros ( m , n )
    //   primematrix = [2,3,5,7,11,13,17,19,23,29]
    //   for k = 1 : m
    //     for i = 1 : n
    //       basis = primematrix ( i )
    //       next(k,i) = vdc ( k , basis )
    //     end
    //   end
    // endfunction
    // dim_num = 3;
    // callf = 100;
    // integr=intprb_crudeld(sumfun,dim_num,haltonseq,callf)
    //
    // // Customize ldseq, which has additionnal parameters
    // function next = haltonseq2 ( m , n , skip , leap , prmat )
    //   next = zeros ( m , n )
    //   k = skip + 1
    //   index = 0
    //   for index = 1 : m
    //     for i = 1 : n
    //       basis = prmat ( i )
    //       next(index,i) = vdc ( k , basis )
    //     end
    //     k = k + leap
    //   end
    // endfunction
    // dim_num = 3;
    // primematrix = [2,3,5,7,11,13,17,19,23,29];
    // skip = 10;
    // // Kocis and Whiten recommend leap = 409.
    // leap = 409;
    // ldseq = list(haltonseq2,skip,leap,primematrix);
    // callf = 100;
    // integr=intprb_crudeld(sumfun,dim_num,ldseq,callf)
    //
    // // Customize the function :
    // // use a function which has additionnal parameters.
    // function r = sumfun2 (m,n,x,e,v)
    // r = sqrt(1/v) * (sum(x,"c") - e)
    // endfunction
    // n = 3;
    // func = list(sumfun2,n/2,n/12);
    // integr = intprb_crudeld ( func , n )
    //
    // Authors
    // Copyright (C) 2011-2013 - Michael Baudin
    //   Michael Baudin - 2010 - DIGITEO
    //
    // Bibliography
    //    "Methods of Numerical Integration", Second Edition, Philip Davis, Philip Rabinowitz, Dover, 2007,
    //    GSL/src/monte/plain.c, "Gnu Scientific Library"
    //    genz/software/fort77/mvtexoh.f/RCRUDE, "Crude Monte-Carlo Algorithm with simple antithetic variates and weighted results on restart", Alan Genz

    [lhs, rhs] = argn();
    apifun_checkrhs ( "intprb_crudeld" , rhs , 2 : 6 )
    apifun_checklhs ( "intprb_crudeld" , lhs , 0 : 1 )
    //
    __crudeld_func__ = varargin(1)
    n = varargin(2)
    __crudeld_ldseq__ = apifun_argindefault ( varargin , 3 , "sobol" )
    callf = apifun_argindefault ( varargin , 4 , 1.e3 )
    bounds = apifun_argindefault ( varargin , 5 , [zeros(n,1) ones(n,1)] )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "intprb_crudeld" , __crudeld_func__ ,   "func" ,  1 , [ "function" "list" ] )
    apifun_checktype ( "intprb_crudeld" , n ,      "n" ,     2 , "constant" )
    apifun_checktype ( "intprb_crudeld" , __crudeld_ldseq__ ,  "ldseq" , 3 , [ "string" "function" "list" ] )
    apifun_checktype ( "intprb_crudeld" , callf ,  "callf" , 4 , "constant" )
    apifun_checktype ( "intprb_crudeld" , bounds , "bounds" ,  5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "intprb_crudeld" , n ,       "n" ,       2 )
    if ( typeof(__crudeld_ldseq__) == "string" ) then
        apifun_checkscalar ( "intprb_crudeld" , __crudeld_ldseq__ ,   "ldseq" ,   3 )
    end
    apifun_checkscalar ( "intprb_crudeld" , callf ,   "callf" ,   4 )
    apifun_checkdims   ( "intprb_crudeld" , bounds ,  "bounds" ,    5 , [n 2] )
    //
    // Check content
    apifun_checkgreq ( "intprb_crudeld" , n ,       "n" ,       2 , 1 )
    apifun_checkgreq ( "intprb_crudeld" , callf ,   "callf" ,   4 , 1 )
    //
    lowb = bounds (:,1)
    uppb = bounds (:,2)
    //
    // Generate the experiments
    typlds = typeof(__crudeld_ldseq__)
    if ( typlds == "string" ) then
        u=lowdisc_ldgen ( callf , n , __crudeld_ldseq__ );
    elseif ( typlds == "function" ) then
        // Macro
        u = __crudeld_ldseq__ ( callf , n );
    else
        // List or tlist
        __crudeld_ldseq__f = __crudeld_ldseq__(1); 
        u = __crudeld_ldseq__f ( callf , n , __crudeld_ldseq__(2:$)); 
    end
    if ( size(u) <> [callf n] ) then
        msg = msprintf(gettext("%s: Error: The expected shape of u from the random number generator is [%d,%d], while computed shape is [%d,%d].") , "intprb_crudeld",callf,n,size(u,"r"),size(u,"c"));
        error ( msg )
    end
    //
    x = ones(callf,1) * lowb' + (ones(callf,1) * (uppb - lowb)') .*u;
    if ( typeof(__crudeld_func__) == "list" ) then
        // List or tlist
        __crudeld_func__f = __crudeld_func__(1); 
        y = __crudeld_func__f ( callf , n , x , __crudeld_func__(2:$)); 
    elseif ( typeof(__crudeld_func__) == "function" ) then
        // Macro or compiled macro
        y = __crudeld_func__ ( callf , n , x );
    end
    if ( size(y) <> [callf 1] ) then
        msg = msprintf(gettext("%s: Error: The expected shape of y = func(m,n,x) is [%d,%d], while computed shape is [%d,%d].") , "intprb_crudeld",m,1,size(y,"r"),size(y,"c"));
        error ( msg )
    end
    volume = prod ( uppb - lowb )
    mu = mean(y)
    integr = volume * mu
endfunction

