// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ integr , accur , varf ] = intprb_crudemc ( varargin )
  // Estimates a multidimensional integral using Monte Carlo.
  //
  // Calling Sequence
  //   integr = intprb_crudemc ( func , n )
  //   integr = intprb_crudemc ( func , n , callf )
  //   integr = intprb_crudemc ( func , n , callf , bounds )
  //   integr = intprb_crudemc ( func , n , callf , bounds , randgen )
  //   [ integr , accur , varf ] = intprb_crudemc ( ... )
  //
  // Parameters
  //   func : a function or a list, the function to be evaluated. If func is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end. See below for details.
  //   n: a 1-by-1 matrix of floating point integers, the spatial dimension.
  //   callf : a 1-by-1 matrix of floating point integers, the number of calls to the function (default = 1.e4)
  //   bounds : a n-by-2 matrix of doubles, where lowb = bounds(:,1) = lower bound and uppb = bounds(:,2) = upper bound of integration (default = [zeros(n,1) ones(n,1)])
  //   randgen : a function or a list, the random number generator (default = grand). See below for details.
  //   integr : a 1-by-1 matrix of doubles, the approximate value of the integral, the mean of the function.
  //   accur : a 1-by-1 matrix of doubles, the estimated error on the integral.
  //   varf : a 1-by-1 matrix of doubles, the approximate value of the variance of f.
  //
  // Description
  // The algorithm uses a crude Monte-Carlo method to estimate the integral.
  //
  // The function should have header
  //
  // <screen>
  // y = func( m , n , x ) 
  // </screen>
  //
  // where 
  // n is a floating point representing the spatial dimension, 
  // m is a floating point integer representing the number of experiments to perform and
  // x is a m x n matrix of doubles.
  // 
  // It might happen that the function requires additionnal 
  // arguments to be evaluated.
  // In this case, we can use the following feature.
  // The argument func can also be a list, where the first 
  // item is a function f.
  // The function f must have the header 
  //
  // <screen>
  // y = f ( m , n , x , a1 , a2 , ... ).
  // </screen>
  //
  // In this case the func variable should hold the list (f,a1,a2,...) and the 
  // input arguments a1, a2, will be automatically be appended at the 
  // end of the calling sequence of f.
  //
  // The uniform random number generator must have header 
  //
  // <screen>
  // u = randgen ( m , n ) 
  // </screen>
  //
  // where m is a 1-by-1 matrix of floating point integers, the number of experiments to be performed
  // and u is a m x n matrix of doubles in the interval [0,1].
  //
  // It might happen that the random number generator requires additionnal 
  // arguments to be evaluated.
  // In this case, we can use the following feature.
  // The argument randgen can also be a list, where the first 
  // element rg is a function. 
  // This function rg must have the header :
  //
  // <screen>
  // u = rg ( m , n , a1 , a2 , ... )
  // </screen>
  //
  // In this case the randgen variable should hold the list (rg,a1,a2,...) and the 
  // input arguments a1, a2, will be automatically be appended at the 
  // end of the calling sequence of rg.
  //
  // If an optional input argument is equal to the empty matrix, it 
  // is set to its default value.
  //
  // The error is estimated as 
  //
  // <screen>
  // accur = volume * varf / sqrt(callf), 
  // </screen>
  //
  // where varf is the standard
  // deviation associated with the outputs of the function. 
  // The Monte-Carlo estimate follows a Normal (Laplace-Gauss) law,
  // with standard deviation accur. 
  // The value of accur ensures that the probability that the actual integral is 
  // in the interval [integr-accur,integr+accur] is greater than 68%. 
  // The probability is 95% that the exact integral is in the range [integr-2*accur,integr+2*accur]. 
  //
  // If the randgen input argument is the empty matrix, the grand function is used to produce 
  // the uniform random numbers.
  //
  // Examples
  // // A basic example
  // function r = sumfun (m,n,x)
  //   // sum of n variables
  //   // Expectation = 0, Variance = 1
  //   v = n/12
  //   e = n/2
  //   r = sqrt(1/v) * (sum(x,"c") - e)
  // endfunction
  // grand("setsd",123456);
  // n = 10;
  // // Exact integral is 0.
  // [ integr , accur , varf ] = intprb_crudemc ( sumfun , n )
  //
  // // Customize the bounds
  // // integral_10^30 sqrt(x)  = 88.46266043324404
  // // E(f)=integral_10^30 sqrt(x)*1/20=4.423133021662201
  // // V(f)=integral_10^30 (sqrt(x)-E(f))^2*1/20 
  //        =0.4358942726814049
  // function r = sqrtfun (m,n,x)
  //   r = sqrt(x)
  // endfunction
  // grand("setsd",123456);
  // n = 1;
  // callf = [];
  // lowb = 10;
  // uppb = 30;
  // bounds = [lowb uppb];
  // // Exact integral is 88.46266043324404.
  // [integr,accur,varf]=intprb_crudemc(sqrtfun,n,callf,bounds)
  //
  // // Customize the random number generator
  // function r = shrand ( m , n )
  //   global shrageseed
  //   r = zeros(m,n)
  //   for k = 1 : m
  //     for i = 1 : n
  //       [value,shrageseed]=intprb_shragerandom(shrageseed);
  //       r(k,i) = value
  //     end
  //   end
  // endfunction
  // global shrageseed;
  // shrageseed = 123456;
  // n = 3;
  // callf = 100;
  // bounds = [];
  // [integr,accur,varf]=intprb_crudemc(sumfun,n,callf,bounds,shrand)
  //
  // // Customize the random number generator :
  // // configure a random number generator which has optionnal arguments.
  // function u = mygen ( m , n , seed , gentype )
  //   grand ( "setgen" , gentype )
  //   grand ( "setsd" , seed )
  //   u = grand ( m , n , "def" )
  // endfunction
  // n = 3;
  // callf = 1000;
  // bounds = [];
  // randgen = list(mygen,123456,"urand");
  // [integr,accur,varf]=intprb_crudemc(sumfun,n,callf,bounds,randgen)
  //
  // // Customize the function :
  // // use a function which has additionnal parameters.
  // function r = sumfun2 (m,n,x,e,v)
  //     r = sqrt(1/v) * (sum(x,"c") - e)
  // endfunction
  // n = 3;
  // func = list(sumfun2,n/2,n/12);
  // [ integr , accur , varf ] = intprb_crudemc ( func , n )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //
  // Bibliography
  //    "Methods of Numerical Integration", Second Edition, Philip Davis, Philip Rabinowitz, Dover, 2007,
  //    GSL/src/monte/plain.c, "Gnu Scientific Library"
  //    genz/software/fort77/mvtexoh.f/RCRUDE, "Crude Monte-Carlo Algorithm with simple antithetic variates and weighted results on restart", Alan Genz

  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_crudemc" , rhs , 2 : 5 )
  apifun_checklhs ( "intprb_crudemc" , lhs , 0 : 3 )
  //
  // Define a default random number generator
  function x=__mcrandgen_default__(m,n)
    x=grand ( m , n , "def")
  endfunction
  //
  __crudemc_func__ = varargin(1)
  n = varargin(2)
  callf = apifun_argindefault ( varargin , 3 , 1.e4 )
  bounds = apifun_argindefault ( varargin , 4 , [zeros(n,1) ones(n,1)] )
  __crudemcrandgen__ = apifun_argindefault ( varargin , 5 , __mcrandgen_default__ )
  //
  // Check input arguments
  //
  // Check type
  apifun_checktype ( "intprb_crudemc" , __crudemc_func__ ,    "func" ,    1 , [ "function" "list" ] )
  apifun_checktype ( "intprb_crudemc" , n ,       "n" ,       2 , "constant" )
  apifun_checktype ( "intprb_crudemc" , callf ,   "callf" ,   3 , "constant" )
  apifun_checktype ( "intprb_crudemc" , bounds ,  "bounds" ,  4 , "constant" )
  apifun_checktype ( "intprb_crudemc" , __crudemcrandgen__ , "randgen" , 5 , [ "function" "list" ] )
  //
  // Check size
  apifun_checkscalar ( "intprb_crudemc" , n ,       "n" ,       2 )
  apifun_checkscalar ( "intprb_crudemc" , callf ,   "callf" ,   3 )
  apifun_checkdims   ( "intprb_crudemc" , bounds ,  "bounds" ,  4 , [n 2] )
  //
  // Check content
  apifun_checkgreq ( "intprb_montecarlo" , n ,       "n" ,       2 , 1 )
  apifun_checkgreq ( "intprb_montecarlo" , callf ,   "callf" ,   3 , 1 )
  //
  lowb = bounds (:,1)
  uppb = bounds (:,2)
  //
  if ( typeof(__crudemcrandgen__) == "list" ) then  
    // List
    __crudemcrandgen__f = __crudemcrandgen__(1); 
    u = __crudemcrandgen__f ( callf , n , __crudemcrandgen__(2:$)); 
  else
    // Macro or compiled macro
    u = __crudemcrandgen__ ( callf , n );
  end
  if ( size(u) <> [callf n] ) then
    msg = msprintf(gettext("%s: Error: The expected shape of u from the random number generator is [%d,%d], while computed shape is [%d,%d].") , "intprb_crudemc",callf,n,size(u,"r"),size(u,"c"));
    error ( msg )
  end
  x = ones(callf,1) * lowb' + (ones(callf,1) * (uppb - lowb)') .*u;
  if ( typeof(__crudemc_func__) == "list" ) then  
    // List or tlist
    __crudemc_func__f = __crudemc_func__(1); 
    y = __crudemc_func__f ( callf , n , x , __crudemc_func__(2:$)); 
  else
    // Macro or compiled macro
    y = __crudemc_func__ ( callf , n, x );
  end
  if ( size(y) <> [callf 1] ) then
    msg = msprintf(gettext("%s: Error: The expected shape of y = func(m,n,x) is [%d,%d], while computed shape is [%d,%d].") , "intprb_crudemc",m,1,size(y,"r"),size(y,"c"));
    error ( msg )
  end
  volume = prod ( uppb - lowb )
  mu = mean(y)
  integr = volume * mu
  // Do not use the variance function : saves the evaluation of mu
  varf = sum((y-mu).^2) / (callf - 1)
  accur = volume * sqrt(varf) / sqrt(callf - 1)
endfunction
