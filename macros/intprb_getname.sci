// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function name = intprb_getname ( varargin )
  // Returns the name.
  //
  // Calling Sequence
  //   name = intprb_getname()
  //   name = intprb_getname(prbmat)
  //   name = intprb_getname(prbmat,longflag)
  //
  // Parameters
  //   prbmat: a m-by-1 matrix of matrix doubles, integer values, the indices of the problem 
  //   longflag : a 1-by-1 matrix of booleans, if true returns the "long name" - human readable - of the test case. If false, returns the short name. (default = %f)
  //   name: a m-by-1 matrix of strings, the names of the problems
  //
  // Description
  // Selects the appropriate function based on nprob.
  // If no argument is provided, returns all the short names available in the data base.
  //
  // See the "Integration Problems User's Manual" available in the doc 
  // directory of this toolbox for a detailed description of this function.
  //
  // Examples
  //   // Get all short names
  //   name = intprb_getname()
  //
  //   // Get the name for Sum test case
  //   nprob = 1
  //   name = intprb_getname(nprob)
  //
  //   // Get all names at once
  //   nprobmax = intprb_getproblems();
  //   name = intprb_getname(1:nprobmax)
  //
  //   // Given a short name, find the problem number
  //   shortname = "roosarnold1"
  //   nprob = find(intprb_getname()==shortname)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2011 - Michael Baudin
  //
  // Bibliography
  //   "Integration Problems User's Manual", Michael Baudin, 2010

  [lhs,rhs]=argn();
  apifun_checkrhs ( "intprb_getname" , rhs , 0 : 2 )
  apifun_checklhs ( "intprb_getname" , lhs , 0 : 1 )
  //
  if ( rhs < 1 ) then
    prbmat = []
  else
    prbmat = varargin(1)
  end
  //
  if ( rhs < 2 ) then
    longflag = %f
  else
    longflag = varargin(2)
  end
  //
  namematrix = [
    "sum"
    "sqsum"
    "sumsqroot"
    "prodones"
    "prodexp"
    "prodcub"
    "prodx"
    "sumfifj"
    "sumf1fj"
    "hellekalek"
    "roosarnold1"
    "roosarnold2"
    "roosarnold3"
    "rst1"
    "rst2"
    "rst3"
    "sobolprod"
    "oscill"
    "prpeak"
    "corpeak"
    "gaussian"
    "c0"
    "discont"
    ];
  //
  // If the user only requested the list of names, return it.
  if ( prbmat == [] ) then
    name = namematrix
    return
  end
  //
  // Check input arguments
  apifun_checktype   ( "intprb_getname" , prbmat ,      "prbmat" ,     1 , "constant" )
  nbp = size(prbmat,"*")
  apifun_checkvecrow ( "intprb_getname" , prbmat ,      "prbmat" ,     1 , nbp );
  apifun_checktype   ( "intprb_getname" , longflag ,    "longflag" ,   2 , "boolean" )
  apifun_checkscalar ( "intprb_getname" , longflag ,    "longflag" ,   2 )
  //
  nprobmax = size ( namematrix , "*" )
  //
  k = 1
  for nprob = prbmat
    if ( nprob < 1 | nprob > nprobmax ) then
      error(msprintf("%s: The input argument nprob is %d but should be between 1 and %d.\n","intprb_getname",nprob,nprobmax))
    end
    if ( longflag == %f ) then
      name(k) = namematrix(nprob)
    else
      shortname = namematrix(nprob)
      execstr("longname = intprb_" + shortname + "([],[],[],[],5)")
      name(k) = longname
    end
    k = k + 1
  end
endfunction

